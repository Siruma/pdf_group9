// C++ code
//

#define buttonPin 3

boolean buttonOn = true; // store the button state
int buttonState = 0;
void setup()
{
  pinMode(4, OUTPUT); // configure the LED as an output
  pinMode(buttonPin, INPUT); // configure the button as an input
  //pinMode(2, OUTPUT); // configure piezo as output
  Serial.begin(9600);
}

void loop()
{

  buttonState = digitalRead(buttonPin);
  if (buttonState == HIGH) {
    if (buttonState == HIGH) {
      // turn LED on:
      delay(25);
      if (buttonOn) {
        buttonOn = false;
        delay(500);
      } else {
        // turn LED off:
        buttonOn = true;
      }
      delay(500); 
    }
  }
  if (buttonOn) {
    digitalWrite(4, LOW); // turn LED off
  }
  else {
    digitalWrite(4, HIGH); // turn LED on
    //tone(2, 1000); // make piezo buzz
    //delay(1000); // wait 1s
    //noTone(2); // stop sound
    //delay(1000); // wait 1s
  }
}
