// C++ code
//

#define MOTOR_EN_1_2  10
#define MOTOR_IN1     9
#define MOTOR_IN2     8
#define echoPin 6 // attach pin D2 Arduino to pin Echo of HC-SR04
#define trigPin 5 //attach pin D3 Arduino to pin Trig of HC-SR04

#define slow 64
#define normal 128
#define fast 255

// defines variables
long duration; // variable for the duration of sound wave travel
int distance; // variable for the distance measurement
int Speed;
boolean buttonOn = true; // store the button state


void Forward_Rev(void){
  analogWrite(MOTOR_EN_1_2, Speed);
  digitalWrite(MOTOR_IN1, HIGH);
  digitalWrite(MOTOR_IN2, LOW);
}

void Backward_Rev(void){
  analogWrite(MOTOR_EN_1_2, Speed);
  digitalWrite(MOTOR_IN1, LOW);
  digitalWrite(MOTOR_IN2, HIGH);
}


void Forward_ramp_up(void){
  digitalWrite(MOTOR_IN1, HIGH);
  digitalWrite(MOTOR_IN2, LOW);
  for (int i=0; i<255; i++) { analogWrite(MOTOR_EN_1_2, i); delay(10); } } void Forward_ramp_down(void){ digitalWrite(MOTOR_IN1, HIGH); digitalWrite(MOTOR_IN2, LOW); for (int i=255; i>=0; i--) {
    analogWrite(MOTOR_EN_1_2, i);
    delay(10);
  }
}

void Backward_ramp_up(void){
  digitalWrite(MOTOR_IN1, LOW);
  digitalWrite(MOTOR_IN2, HIGH);
  for (int i=0; i<255; i++) { analogWrite(MOTOR_EN_1_2, i); delay(10); } } void Backward_ramp_down(void){ digitalWrite(MOTOR_IN1, LOW); digitalWrite(MOTOR_IN2, HIGH); for (int i=255; i>=0; i--) {
    analogWrite(MOTOR_EN_1_2, i);
    delay(10);
  }
}

void Brake(void){
  digitalWrite(MOTOR_IN1, HIGH);
  digitalWrite(MOTOR_IN2, HIGH);
}

//SETUP
void setup(){
   pinMode(4, OUTPUT); // configure the LED as an output
   pinMode(3, INPUT); // configure the button as an input
   pinMode(2, OUTPUT); // configure piezo as output
   pinMode(MOTOR_EN_1_2, OUTPUT);
   pinMode(MOTOR_IN1, OUTPUT);
   pinMode(MOTOR_IN2, OUTPUT);
   pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
   pinMode(echoPin, INPUT); // Sets the echoPin as an INPUT
   Serial.begin(9600); // // Serial Communication is starting with 9600 of baudrate speed
}


void loop(){
 Speed=normal;
  // Clears the trigPin condition
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin HIGH (ACTIVE) for 10 microseconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
  // Displays the distance on the Serial Monitor
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.println(" cm");
 if(digitalRead(3)) {
 	delay(25);
 	if(digitalRead(3)) {
 // if button was pressed (and was not a spurious signal)
      if(buttonOn){
 		// toggle button state
 			buttonOn = false; 
        	delay(1000);
      }
 		else 
 			buttonOn = true;
 	delay(500); // wait 0.5s -- don't run the code multiple times
 } 
 } 
 if(buttonOn) 
 	digitalWrite(4, LOW); // turn LED off
 else {
   		digitalWrite(4, HIGH); // turn LED on
   		tone(2, 1000); // make piezo buzz
   		Forward_Rev(); 
    	delay(1000); // wait 1s
   		Brake();
    	noTone(2); // stop sound
    	delay(1000); // wait 1s
   		Backward_Rev(); 
 }
}