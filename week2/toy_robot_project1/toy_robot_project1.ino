// C++ code
//
#include <LiquidCrystal.h>
#define echoPin 2 // attach pin D2 Arduino to pin Echo of HC-SR04
#define trigPin 3 //attach pin D3 Arduino to pin Trig of HC-SR04

// defines variables
long duration; // variable for the duration of sound wave travel
int distance; // variable for the distance measurement
LiquidCrystal lcd(5, 6, 10,11,12,13);
boolean buttonOn = false; // store the button state
void setup() {
 // put your setup code here, to run once:
 lcd.begin(16, 2);
 pinMode(7, OUTPUT); // configure the LED as an output
 pinMode(4, INPUT); // configure the button as an input
 pinMode(9, OUTPUT); // configure piezo as output
 pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
 pinMode(echoPin, INPUT); // Sets the echoPin as an INPUT
 Serial.begin(9600); // // Serial Communication is starting with 9600 of baudrate speed
}
void loop() {
 // put your main code here, to run repeatedly:
  // Clears the trigPin condition
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin HIGH (ACTIVE) for 10 microseconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
  lcd.setCursor(0, 0); // Sets the location at which subsequent text written to the LCD will be displayed
  lcd.print("Distance: "); // Prints string "Distance" on the LCD
  lcd.print(distance); // Prints the distance value from the sensor
  lcd.print(" cm");
  delay(10);
 if(digitalRead(4)) {
 delay(25);
 if(digitalRead(4)) {
 // if button was pressed (and was not a spurious signal)
 if(buttonOn)
 // toggle button state
 buttonOn = false; 
 else 
 buttonOn = true;
 delay(500); // wait 0.5s -- don't run the code multiple times
 } 
 } 
 if(buttonOn) 
 digitalWrite(7, LOW); // turn LED off
  else {
 	digitalWrite(7, HIGH); // turn LED on
 	tone(9, 1000); // make piezo buzz
    delay(1000); // wait 1s
 	noTone(9); // stop sound
 	delay(1000); // wait 1s
  }
}