// C++ code
//
#include <LiquidCrystal.h>

#define MOTOR_EN_1_2 10
#define MOTOR_EN_3_4 13
#define MOTOR_IN1 9
#define MOTOR_IN2 8
#define MOTOR_IN3 11
#define MOTOR_IN4 12
#define echoPin 6 // attach pin D6 Arduino to pin Echo of HC-SR04
#define trigPin 5 // attach pin D5 Arduino to pin Trig of HC-SR04
#define ledPin 4 // attach pin D4 Arduiro to Led
#define button1Pin 3 // attach pin D3 Arduiro to button 1
#define button2Pin 1 // attach pin D1 Arduiro to button 2
#define piezoPin 2 // attach pin D2 Ardoiro to piezo

#define slow 64
#define normal 128
#define fast 255

// defines variables
long duration; // variable for the duration of sound wave travel
int distance;  // variable for the distance measurement
int Speed;
boolean buttonOn = true; // store the button state

// LiquidCrystal lcd(5, 6, 10,11,12,13);

unsigned long previousTimeBuzzer = millis();
long timeIntervaldeBuzzer = 1000;

unsigned long previousTimeReadDist = millis();
long timeIntervaldeReadDist = 2000;

unsigned long previousTimeButton25 = millis();
long timeIntervaldeButton25 = 25;

unsigned long previousTimeButton1000 = millis();
long timeIntervaldeButton1000 = 1000;

unsigned long previousTimeButton500 = millis();
long timeIntervaldeButton500 = 500;

unsigned long previousTimeUltraSonic2 = micros();
long timeIntervaldeUltraSonic2 = 2;

unsigned long previousTimeUltraSonic10 = micros();
long timeIntervaldeUltraSonic10 = 10;


unsigned long currentTimeMill = millis();
unsigned long currentTimeMicro = micros();

void Forward_Rev(void)
{
	analogWrite(MOTOR_EN_1_2, Speed);
  	analogWrite(MOTOR_EN_3_4, Speed);
	digitalWrite(MOTOR_IN1, HIGH);
	digitalWrite(MOTOR_IN3, HIGH);
	digitalWrite(MOTOR_IN2, LOW);
	digitalWrite(MOTOR_IN4, LOW);
}

void Backward_Rev(void)
{
	analogWrite(MOTOR_EN_1_2, Speed);
  	analogWrite(MOTOR_EN_3_4, Speed);
	digitalWrite(MOTOR_IN1, LOW);
	digitalWrite(MOTOR_IN3, LOW);
	digitalWrite(MOTOR_IN2, HIGH);
	digitalWrite(MOTOR_IN4, HIGH);
}

void Turn_Forward(void){
	analogWrite(MOTOR_EN_1_2, Speed);
	digitalWrite(MOTOR_IN1, HIGH);
	digitalWrite(MOTOR_IN2, LOW);
}

void Break(void)
{
	digitalWrite(MOTOR_IN1, HIGH);
	digitalWrite(MOTOR_IN2, HIGH);
	digitalWrite(MOTOR_IN3, HIGH);
	digitalWrite(MOTOR_IN4, HIGH);
}

void readUltraSonic()
{
	// Clears the trigPin condition
	digitalWrite(trigPin, LOW);
	if (currentTimeMicro - previousTimeUltraSonic2 > timeIntervaldeUltraSonic2)
	{
		// Sets the trigPin HIGH (ACTIVE) for 10 microseconds
		previousTimeUltraSonic2 = currentTimeMicro;
		digitalWrite(trigPin, HIGH);

		if (currentTimeMicro - previousTimeUltraSonic10 > timeIntervaldeUltraSonic10)
		{
			previousTimeUltraSonic10 = currentTimeMicro;
			digitalWrite(trigPin, LOW);
			// Reads the echoPin, returns the sound wave travel time in microseconds
			duration = pulseIn(echoPin, HIGH);
			// Calculating the distance
			distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
			// Displays the distance on the Serial Monitor
			// Serial.print("Distance: ");
			// Serial.print(distance);
			// Serial.println(" cm");
		}
	}
}

void ledAndBuzzer()
{
	if (buttonOn)
		digitalWrite(ledPin, LOW); // turn LED off
	else
	{
		digitalWrite(ledPin, HIGH); // turn LED on
		tone(piezoPin, 1000);		   // make piezo buzz
		if (currentTimeMill - previousTimeBuzzer > timeIntervaldeBuzzer)
		{ // wait 1s
			previousTimeBuzzer = currentTimeMill;
			noTone(piezoPin); // stop sound
		}
	}
}

void writeLCD()
{
	//lcd.setCursor(0, 0);	 // Sets the location at which subsequent text written to the LCD will be displayed
	//lcd.print("Distance: "); // Prints string "Distance" on the LCD
	//lcd.print(distance);	 // Prints the distance value from the sensor
	//lcd.print(" cm");
}

// SETUP
void setup()
{
	// lcd.begin(16, 2); //Configure the LCD
	pinMode(ledPin, OUTPUT); // configure the LED as an output
	pinMode(button1Pin, INPUT);	// configure the button 1 as an input
  	pinMode(button2Pin, INPUT);	// configure the button 2 as an input
	pinMode(piezoPin, OUTPUT); // configure piezo as output
	pinMode(MOTOR_EN_1_2, OUTPUT);
	pinMode(MOTOR_IN1, OUTPUT);
	pinMode(MOTOR_IN2, OUTPUT);
  	pinMode(MOTOR_EN_3_4, OUTPUT);
	pinMode(MOTOR_IN3, OUTPUT);
	pinMode(MOTOR_IN4, OUTPUT);
	pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
	pinMode(echoPin, INPUT);  // Sets the echoPin as an INPUT
	Serial.begin(9600);		  // // Serial Communication is starting with 9600 of baudrate speed
}

void loop()
{
	Speed = normal;
	currentTimeMill = millis();
	currentTimeMicro = micros();
	if (digitalRead(button1Pin)|| digitalRead(button2Pin))
	{
		if (currentTimeMill - previousTimeButton25 > timeIntervaldeButton25)
		{
			previousTimeButton25 = currentTimeMill;
			if ((digitalRead(button1Pin)|| digitalRead(button2Pin)) && currentTimeMill - previousTimeButton500 > timeIntervaldeButton500)
			{
				previousTimeButton500 = currentTimeMill;
				// if button was pressed (and was not a spurious signal)
				if (buttonOn && currentTimeMill - previousTimeButton1000 > previousTimeButton1000)
				{
					previousTimeButton1000 = currentTimeMill;
					// toggle button state
					buttonOn = false;
				}
				else
					buttonOn = true;
			}
		}
	}

	if (currentTimeMill - previousTimeReadDist > timeIntervaldeReadDist)
	{
		previousTimeReadDist = currentTimeMill;
		readUltraSonic();
	}

	if (currentTimeMill - previousTimeBuzzer > timeIntervaldeBuzzer)
	{
		previousTimeBuzzer = currentTimeMill;
		ledAndBuzzer();
	}

	if (!buttonOn)
	{
		Forward_Rev();
		if (distance < 2)
		{
			unsigned long previousTime = mills();
			Break();
			Backward_Rev();
			if(currentTimeMill - previousTime > 1000){
				previousTime = currentTimeMill;
				Break();
			}
			Turn_Forward();
			if(currentTimeMill - previousTime > 1000){
				Break();
			}
		}
		Forward_Rev();
	}
	else
		Break();
}