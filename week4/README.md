Week 4

Diary of week 4:
 - Samuli finalized the schematic for the robot 
 - Samuli added second motor and button to the code
 - Pyry modified the 2D-design files
 - Iiris and Oona used the laser cutter to cut the parts
 - Iiris modified the 2D-models some more to make the ultrasonic sensor and the wheels fit
