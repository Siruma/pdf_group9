// C++ code
//
#include <DFRobot_RGBLCD1602.h>

#define MOTOR_EN_1_2 9
#define MOTOR_EN_3_4 10
#define MOTOR_IN1 8
#define MOTOR_IN2 7
#define MOTOR_IN3 11
#define MOTOR_IN4 12
#define echoPin 6 // attach pin D6 Arduino to pin Echo of HC-SR04
#define trigPin 5 // attach pin D5 Arduino to pin Trig of HC-SR04
#define ledPin 4 // attach pin D4 Arduiro to Led
#define button1Pin 3 // attach pin D3 Arduiro to button 1
#define piezoPin 2 // attach pin D2 Ardoiro to piezo

#define slow 64
#define normal 128
#define fast 255

#define clockTimeBase 10

// defines variables
long duration; // variable for the duration of sound wave travel
int distance;  // variable for the distance measurement
int Speed;
boolean buttonOn = true; // store the button state
int button1State = 0;

DFRobot_RGBLCD1602 lcd(/*lcdCols*/16,/*lcdRows*/2);  //16 characters and 2 lines of show

unsigned long previousTimeBuzzer = millis();
long timeIntervaldeBuzzer = 1000;

unsigned long previousTimeReadDist = millis();
long timeIntervaldeReadDist = 2000;

unsigned long previousTimeButton25 = millis();
long timeIntervaldeButton25 = 25;

unsigned long previousTimeButton1000 = millis();
long timeIntervaldeButton1000 = 1000;

unsigned long previousTimeButton500 = millis();
long timeIntervaldeButton500 = 500;

unsigned long previousTimeUltraSonic2 = micros();
long timeIntervaldeUltraSonic2 = 2;

unsigned long previousTimeUltraSonic10 = micros();
long timeIntervaldeUltraSonic10 = 10;


unsigned long previousTimePrint = millis();
long timeIntervaldePrint = 1000;


unsigned long currentTimeMill = millis();
unsigned long currentTimeMicro = micros();

int clockTime = clockTimeBase; //timer

//Motor Functions
void Forward_Rev(void)
{
  analogWrite(MOTOR_EN_1_2, Speed);
  analogWrite(MOTOR_EN_3_4, Speed);
  digitalWrite(MOTOR_IN1, HIGH);
  digitalWrite(MOTOR_IN3, HIGH);
  digitalWrite(MOTOR_IN2, LOW);
  digitalWrite(MOTOR_IN4, LOW);
}

void Backward_Rev(void)
{
  analogWrite(MOTOR_EN_1_2, Speed);
  analogWrite(MOTOR_EN_3_4, Speed);
  digitalWrite(MOTOR_IN1, LOW);
  digitalWrite(MOTOR_IN3, LOW);
  digitalWrite(MOTOR_IN2, HIGH);
  digitalWrite(MOTOR_IN4, HIGH);
}

void Turn_Forward(void) {
  analogWrite(MOTOR_EN_1_2, Speed);
  digitalWrite(MOTOR_IN1, HIGH);
  digitalWrite(MOTOR_IN2, LOW);
}

void Break(void)
{
  digitalWrite(MOTOR_IN1, HIGH);
  digitalWrite(MOTOR_IN2, HIGH);
  digitalWrite(MOTOR_IN3, HIGH);
  digitalWrite(MOTOR_IN4, HIGH);
}

//Ultrasonic sensor
void readUltraSonic()
{
  // Clears the trigPin condition
  digitalWrite(trigPin, LOW);
  if (currentTimeMicro - previousTimeUltraSonic2 > timeIntervaldeUltraSonic2)
  {
    // Sets the trigPin HIGH (ACTIVE) for 10 microseconds
    previousTimeUltraSonic2 = currentTimeMicro;
    digitalWrite(trigPin, HIGH);

    if (currentTimeMicro - previousTimeUltraSonic10 > timeIntervaldeUltraSonic10)
    {
      previousTimeUltraSonic10 = currentTimeMicro;
      digitalWrite(trigPin, LOW);
      // Reads the echoPin, returns the sound wave travel time in microseconds
      duration = pulseIn(echoPin, HIGH);
      // Calculating the distance
      distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
      //Displays the distance on the Serial Monitor
      writeLCD();
      Serial.print("Distance: ");
      Serial.print(distance);
      Serial.println(" cm");
    }
  }
}


//Truning on and off the buzzer and LEDs. 
//Also writing to the LCD
void ledAndBuzzer()
{
  if (buttonOn) {
    digitalWrite(ledPin, LOW); // turn LED off
    //clockTime = clockTimeBase;
    noTone(piezoPin);
    //lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Push the Button!");
  }
  else
  {
    if (currentTimeMill - previousTimePrint > timeIntervaldePrint && clockTime > -1) {
      previousTimePrint = currentTimeMill;
      int minutes = clockTime / 60;
      int seconds = clockTime % 60;
      //print timer to LCD
      lcd.setCursor(0, 0);
      lcd.print("TIME: ");
      lcd.print(minutes);
      lcd.print(" : ");
      lcd.print(seconds);
      lcd.print("    ");
      clockTime--;
    }
    if (clockTime < 0) {
      digitalWrite(ledPin, HIGH); // turn LED on
      tone(piezoPin, 1000);       // make piezo buzz
      if (currentTimeMill - previousTimeBuzzer > timeIntervaldeBuzzer)
      { // wait 1s
        previousTimeBuzzer = currentTimeMill;
        noTone(piezoPin); // stop sound
      }
    }
  }
}

//Funtion to write the Distance to the LCD
void writeLCD()
{
  lcd.setCursor(0, 1);	 // Sets the location at which subsequent text written to the LCD will be displayed
  lcd.print("Distance: "); // Prints string "Distance" on the LCD
  lcd.print(distance);	 // Prints the distance value from the sensor
  lcd.print(" cm   ");
}

// SETUP
void setup()
{
  lcd.init(); //Configure the LCD
  pinMode(ledPin, OUTPUT); // configure the LED as an output
  pinMode(button1Pin, INPUT);	// configure the button 1 as an input
  pinMode(piezoPin, OUTPUT); // configure piezo as output
  pinMode(MOTOR_EN_1_2, OUTPUT);
  pinMode(MOTOR_IN1, OUTPUT);
  pinMode(MOTOR_IN2, OUTPUT);
  pinMode(MOTOR_EN_3_4, OUTPUT);
  pinMode(MOTOR_IN3, OUTPUT);
  pinMode(MOTOR_IN4, OUTPUT);
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin, INPUT);  // Sets the echoPin as an INPUT
  Serial.begin(9600);		  // // Serial Communication is starting with 9600 of baudrate speed
  Break();
}

//MAIN LOOP
void loop()
{
  Speed = normal;
  currentTimeMill = millis();
  currentTimeMicro = micros();
  button1State = digitalRead(button1Pin);
  if (button1State == HIGH )
  {
    if (currentTimeMill - previousTimeButton25 > timeIntervaldeButton25)
    {
      previousTimeButton25 = currentTimeMill;
      if ((button1State == HIGH ) && currentTimeMill - previousTimeButton500 > timeIntervaldeButton500)
      {
        previousTimeButton500 = currentTimeMill;
        // if button was pressed (and was not a spurious signal)
        if (buttonOn && currentTimeMill - previousTimeButton1000 > previousTimeButton1000)
        {
          previousTimeButton1000 = currentTimeMill;
          // toggle button state
          buttonOn = false;

        }
        else
          buttonOn = true;
          //Add time to timer
          clockTime += clockTimeBase;
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("TIME: ");
          lcd.print(clockTime);
      
      }
    }
  }

  if (currentTimeMill - previousTimeReadDist > timeIntervaldeReadDist)
  {
    previousTimeReadDist = currentTimeMill;
    readUltraSonic();
  }

  if (currentTimeMill - previousTimeBuzzer > timeIntervaldeBuzzer)
  {
    previousTimeBuzzer = currentTimeMill;
    ledAndBuzzer();
  }

  //Controlling the motors
  if (!buttonOn && clockTime < 0)
  {
    Forward_Rev();
    if (distance < 10)
    {
      long previousTime = currentTimeMill;
      Break();
      Turn_Forward();
      if (currentTimeMill - previousTime > 1000) {
        Break();
      }
    }
  }
  else
    Break();
}
